import { Component, render, html, svg, useState, useEffect } from 'uland'

const observer = new IntersectionObserver(listLooker, { threshold: .2 })
const gridOrderMap = new Map([
  [1, 'first'],
  [2, 'second'],
  [3, 'third']
])

addEventListener('DOMContentLoaded', async () => {
  const main = document.body.querySelector('main')
  render(main, App)
  const listItems = document.querySelectorAll('ol li')
  for (const el of listItems) {
    observer.observe(el)
  }
})

async function getContent () {
  return await fetch('/content.json')
    .then(data => data.json())
}

const App = Component((initialState = {}) => {

  const [content, setContent] = useState(initialState)

  useEffect(() => {
    getContent().then(data => setContent(data))
  }, [])

  if (!content.child) return html`<h3 class="p2 o-8">Loading…</h3>`

  return html`
    <main class="bg-backdrop">
      ${Header()}
      <div class="relative dg w-100" style="grid-auto-flow: column; grid-template-columns: 6fr 1fr 1fr 1fr; grid-template-areas: 'image third second first';">
        ${content.child.map(Gallery)}
      </div>
    </main>
  `
})

function folderImageCount(acc, f) {
  if (f.ext === 'jpg') return 1 + acc
  if (f.isDir) acc += f.child.reduce(folderImageCount, 0)
  return acc 
 }

const Gallery = Component(({ ext, path, isDir, child }) => {
  if (ext === 'jpg') return html`${Image(path)}`
  const pArray = path.split('/').reverse()
  const images = child?.filter(m => m.ext === 'jpg')
  const imageCount = child?.reduce(folderImageCount, 0)
  console.log(imageCount)
  let name = pArray[0]
  if (name.split('-').length > 1) name = name.split('-')[1]
  if (isDir && imageCount === 0) return html``
  if (isDir) return html`
    <h3 class="dn m0 bg-backdrop sticky p2 f-2 top-0 z0 js-end writing-vlr" style="${`grid-row: span ${imageCount}; grid-column: ${gridOrderMap.get(pArray.length - 1)}`}">${name}</h3>
    ${child.map(Gallery)}
  `
  return html`` 
})

const Image = Component(src => {
 return html`
    <picture class="top-0 sticky z0 df ss-center h-100vh align-start p0" style="grid-column-start: 1;">
      <source srcset="${src.slice(0, -3) + 'avif'}" type="image/avif"/>
      <source srcset="${src.slice(0, -3) + 'webp'}" type="image/webp"/>
      <source srcset="${src.slice(0, -3) + '.small.jpg'}" type="image/jpeg"/>
      <img class="m0 o-0 to t-ease t-short w-100 fit-cover" src="${src}"
        onload="this.classList.remove('o-0') && this.parentNode.nextElementNode?.firstChildNode.load()" loading="lazy" />
    </picture>
  `
})

function listLooker (entries) {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      console.log(entry)
    }
  })
}

const Header = Component(() => html`
  <header class="z1 fixed df g2 p2 bottom-0 right-0 justify-end mbm-screen">
    <h1 class="m0 tr g2 highlight" style="line-height: .7em">
      ${Logo()}
      <span class="clip absolute">Mira</span>
      <br/>
      <span class="uppercase" style="vertical-align: -.3em;">Ceramics</span>
    </h1>
  </header>
`)

const Logo = Component(() => svg`
  <svg aria-hidden id="logo-swirl" xmlns="https://www.w3c.org/2000/svg" class="m0 h3 overflow-visible" viewbox="0 0 1024 512">
    <style>
      #logo-swirl path {
        stroke: currentColor;
        fill: transparent;
        stroke-width: 34;
        stroke-linecap: round;
        stroke-linejoin: round;
      }
      #mi path {
        stroke-dasharray: 1302 128 1 1680;
      }
      #ra path {
        stroke-dasharray: 500 156 700 1800;
      }
    </style>
    <g id="mi" transform="translate(64 64)">
      <path d="m 0 374 L 0 64 C 0 168, 256 168, 256 0 v 314 c 0 112, 128 112, 128 0 v -220">
        <animate
          attributeName="stroke-dashoffset" 
          dur="0.9s"
          values="1620;0"  
          calcMode="spline"
          keySplines="0.185,0.675,0,0.995"
        />
      </path>
    </g>
    <g id="ra" transform="translate(64 64)">
      <path d="M 382 318 c 0 112, 128 112, 128 0 v -80 v 60 c 0 -64, 256 -128, 256 0 v 12 c 0 124, -128 124, -128 0 c 0 -128, 128 -128, 128 0 c 0 112, 128 112, 128 0" >
        <animate
          attributeName="stroke-dashoffset" 
          dur="0.9s"
          values="1680;0"  
          calcMode="spline"
          keySplines="0.185,0.675,0,0.995"
        />
      </path>
    </g>
  </svg>
`)
