/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    static: '/',
    src: { url: '/dist' },
  },
  plugins: [
  ],
  packageOptions: {
    source: "remote"
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    out: "public",
    clean: false
  },
  optimize: {
    bundle: true,
    minify: true,
    target:' es2018'
  }
  alias: {
    /* ... */
  },
};
