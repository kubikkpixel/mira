# Sigita Kurlyte

![](portrait.jpg)

## Profile
I have a strong arts-background, passion for cooking and natural world. I have over 3 years  experience as a kitchen assistant and a chef. I am resourceful and innovative, always punctual and communicating in a kind manner. 
I am quick to adopt to a new environment and and attentive to the details. 

### Personal info
* Age: 30
* Phone: +45 501 12 742
* E-mail: siga.kur@gmail.com


## Work Experience
| Start | End | Title | Workplace | Location |
|-------|-----|-------|-----------|----------|
| 2019  | 2021| Ceramics Teacher  | Ungdomskole | Køge, Denmark |
| 2019  | 2021| Ceramics Teacher  | FOF | Køge, Denmark |
| 2019  | 2021| Cook | Plant Power Food | Copenhagen, Denmark |
| 2018  | 2019| Kitchen Assistant | Yellow Rose | Copenhagen, Denmark 
| 2017  | 2018| Ceramist, sculptor| UAB Vaizdinys | Panevėžys, Lithuania |
| 2016  | 2017 | Sales associate | GAP | Kaunas, Lithuania |
| 2014  | 2015 | Sales associate | De Licateum | Vilnius, Lithuania|
| 2011  | 2013 | Cash Manager | UAB Novogaming | Vilnius, Lithuania |
| 2010  | 2011 | Ceramic Decorator | UAB Unikalus Vaizdas | Panevėžys, Lithuania |

## Education
* Alytus School of Arts and Crafts
: Ceramic Department - _Lithuania - 2007-2010_
* Anadolu University
: Ceramic Department - _Eskisehir, Turkey - 2014-2015_
* J. Vienožinskis University
: Faculty of Applied Arts / Ceramic Department - _Kaunas, Lithuania - 2013-2016_

### Skills
* Responsible, accurate
* Communicative, calm
* Respectful, easy learning
* Fast, curious 
* Creative, innovative

### Languages
* Lithuanian - native
* English - good
* Russian - basic
* Danish - beginner
