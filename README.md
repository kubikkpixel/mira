# Mira

> ✨ render a hyperdrive as a gallery

Uses [hyper-copy](https://github.com/andrewosh/hypercopy) to fetch the `public/content` folder and [dir2json](https://github.com/husseinraed/dir2json)
to build a content.json that a `src/index.js` render a single page gallery in `public/index.html`

## Available Scripts

### pnpm run update-content

Runs [hyper-copy](https://github.com/andrewosh/hypercopy) and [dir2json](https://github.com/husseinraed/dir2json)
create `public/content.json` that a `src/index.js` render a single page gallery in `public/index.html`

### pnpm run resize

Runs imagemagic convert one-liner on JPG's in content folder

### pnpm start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm run build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

**For the best production performance:** Add a build bundler plugin like [@snowpack/plugin-webpack](https://github.com/snowpackjs/snowpack/tree/main/plugins/plugin-webpack) or [snowpack-plugin-rollup-bundle](https://github.com/ParamagicDev/snowpack-plugin-rollup-bundle) to your `snowpack.config.json` config file.
